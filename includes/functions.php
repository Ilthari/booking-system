<?php
	function clean($string) {
		$string = stripslashes($string);
		$string = mysql_real_escape_string($string);
		return $string;
	}
	
	function is_logged_in() {
		if(isset($_SESSION['id'])) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function is_instructor_logged_in() {
		if(isset($_SESSION['instructor_id'])) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function password_encrypt($string) {
		return sha1($string);
	}
	
	function redirect($string) {
		header("location: " . $string);
	}
	
	function get_page_title($request, $type) {
		if($request=='about') {
			$title = 'About';
		}
		elseif($request=='services') {
			if($type=='pre-test') {
				$title = 'Theory Test Training';
			}
			elseif($type == 'beginner') {
				$title = 'Beginners';
			}
			elseif($type == 'advanced') {
				$title = 'Post Test Tuition';
			}
			else {
				$title = 'Services';
			}
		}
		elseif($request=='contact') {
			$title = 'Contact';
		}
		elseif($request=='sitemap') {
			$title = 'Sitemap';
		}
		elseif($request=='privacy-policy') {
			$title = 'Privacy Policy';
		}
		elseif($request=='instructor') {
			if($type=='login') {
				$title = 'Instructor Login';
			}
			elseif($type=='view-bookings') {
				$title = 'View Bookings';
			}
			elseif($type=='cancel-booking') {
				$title = 'Cancel Booking';
			}
			else {
				$title = 'Instructor';
			}
		}
		elseif($request=='learner') {
			if($type=='login') {
				$title = 'Learner Login';
			}
			elseif($type=='view-bookings') {
				$title = 'View Bookings';
			}
			elseif($type=='make-booking') {
				$title ='Make Booking';
			}
			elseif($type=='cancel-booking') {
				$title = 'Cancel Booking';
			}
			elseif($type=='book') {
				$title = 'Book Lesson';
			}
			elseif($type=='confirm-booking') {
				$title = 'Confirm Booking';
			}
			else {
				$title = 'Learner';
			}
		}
		elseif($request=='') {
			$title = '';
		}
		else {
			$title = 'Error 404 - Not Found';
		}
		
		return (($title!='') ? $title . ' | ' : '' ) . 'Chris James Driving Tuition';
	}
	
?>