CREATE TABLE learners (
	learner_id int auto_increment,
	learner_name varchar(255),
	learner_email varchar(255),
	learner_username varchar(255),
	learner_password varchar(255),
	primary key(learner_id)
);

CREATE TABLE bookings (
	booking_id int auto_increment,
	booking_owner int,
	booking_date varchar(255),
	booking_length varchar(255),
	primary key(booking_id)
);

CREATE TABLE instructors (
	instructor_id int auto_increment,
	instructor_name varchar(255),
	instructor_email varchar(255),
	instructor_username varchar(255),
	instructor_password varchar(255),
	primary key(instructor_id)
);

INSERT INTO learners VALUES ('', 'bob', 'email@something.com', 'learner', 'b879c6e092ce6406eb1f806bf3757e49981974a7');
INSERT INTO instructors VALUES ('', 'Chris James', 'email@something.com', 'instructor', '5db3005d1c92d3def956044087157bb23f29c6b0');