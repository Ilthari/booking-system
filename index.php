<?php
	include '/_config.php';
	include '/includes/functions.php'; 

	error_reporting(0);
	
	session_start();
	$con = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS) or die(mysql_error());
	$db = mysql_select_db(MYSQL_DB) or die(mysql_error());
	
	$request = clean($_GET['a']);
	$type = clean($_GET['b']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="I have been teaching L-driving for over 34 years, originally in London and now in Milton Keynes, and achieve an average annual pass rate of over 70%, compared to a national average of fewer than 43%." />
		<meta name="keywords" content="driving, instructor, school, learn, to, drive, advanced, beginner, passplus" />
		<title><?php echo get_page_title($request, $type); ?></title>
		<link rel="stylesheet" href="/styles/style.css" type="text/css" />
	</head>
	
	<body>
		<div id="container">
			<div id="header">
				<a href="/"><img src="/images/logo.jpg" alt="Chris James Driving Tuition" title="Chris James Driving Tuition" /></a>
			</div>
			<?php if($request != 'learner' && $request != 'instructor') : ?>
				<!-- Main Navigation -->
				<div id="navigation">
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/about">About Me</a></li>
						<li><a href="/services">Services</a>
							<ul>
								<li><a href="/services/pre-test">Theory Test Training</a></li>
								<li><a href="/services/beginner">Beginner</a></li>
								<li><a href="/services/advanced">Post Test Tuition</a></li>
							</ul>
						</li>
						<li><a href="/contact">Contact</a></li>
						<li><?php if(!is_logged_in()) :  ?><a href="/learner/login">Learner Login</a><?php else : ?> <a href="/learner/view-bookings">Learner Panel</a> <?php endif; ?></li>
					</ul>
					<img src="/images/nav-bar-guy-red.jpg" alt="Passed Test" title="Passed Test" id="nav-bar-image" />
				</div>
				<div id="content-container">
					<?php if($request != "contact") :?>
						<div id="content-sidebar">
							<div class="sidebar-module">
								<!-- Animated Advert -->
								<object classid="" width="250" height="250" id="advert">
									<param name="movie" value="/images/advert.swf" />
									<param name="quality" value="high" />
									<param name="bgcolor" value="#ffffff" />
									<param name="play" value="true" />
									<param name="loop" value="true" />
									<param name="wmode" value="window" />
									<param name="scale" value="showall" />
									<param name="menu" value="true" />
									<param name="devicefont" value="false" />
									<param name="salign" value="" />
									<param name="allowScriptAccess" value="sameDomain" />
									<!--[if !IE]>-->
									<object type="application/x-shockwave-flash" data="/images/advert.swf" width="250" height="250">
										<param name="movie" value="/images/advert.swf" />
										<param name="quality" value="high" />
										<param name="bgcolor" value="#ffffff" />
										<param name="play" value="true" />
										<param name="loop" value="true" />
										<param name="wmode" value="window" />
										<param name="scale" value="showall" />
										<param name="menu" value="true" />
										<param name="devicefont" value="false" />
										<param name="salign" value="" />
										<param name="allowScriptAccess" value="sameDomain" />
									<!--<![endif]-->
										<a href="http://www.adobe.com/go/getflash">
											<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
										</a>
									<!--[if !IE]>-->
									</object>
									<!--<![endif]-->
								</object>
							</div>
							<div class="sidebar-module">
								<a href="/services"><img src="/images/services-advert.jpg" alt="Check out our services!" title="Check out our services!" /></a>
							</div>
						</div>
						<div id="content">
							<!-- Content Front-End -->
							<?php
								if($request == '') {
									include '/modules/home.php'; 
								} 
								elseif($request == 'about') {
									include '/modules/about.php'; 
								}
								elseif($request == 'sitemap') {
									include '/modules/sitemap.php';
								}
								elseif($request == 'services') {
									if($type == 'pre-test') {
										include '/modules/services-pre-test.php';
									}
									else if($type == 'beginner') {
										include '/modules/services-beginner.php';
									}
									elseif($type == 'advanced') {
										include '/modules/services-advanced.php';
									}
									else {
										include '/modules/services.php';
									}
								}
								elseif($request == 'privacy-policy') {
									include '/modules/privacy-policy.php'; 
								}
								elseif($request == 'message-sent') {
									include '/modules/message-sent.php'; 
								}
								else {
									include '/modules/error404.php'; 
								}
							?>
						</div>
						<br class="clear" />
					<?php else : ?>
						<div id="full-content">
							<?php
								include 'modules/contact.php'; 
							?>
						</div>
					<?php endif; ?>
				</div>
			<?php elseif($request=='learner') : ?>
				<!-- Request Learner -->
				<div id="navigation">
					<ul>
						<?php if(is_logged_in()) : ?>
							<li><a href="/action.php?type=logout">Logout</a></li>
							<li><a href="/learner/view-bookings">View Bookings</a></li>
							<li><a href="/learner/make-booking">Make Booking</a></li>
						<?php else : ?>
							<li><a href="/">Back Home</a></li>
						<?php endif; ?>
					</ul>
					<img src="/images/nav-bar-guy-red.jpg" alt="Passed Test" id="nav-bar-image" />
				</div>
				<?php if($type == 'login') : ?>
					<!-- Login -->
					<div id="content-container">
						<?php
							include '/modules/learner-login.php';
						?>
					</div>
				<?php elseif($type == 'view-bookings') : ?>
					<?php if(is_logged_in()) : ?>
						<!-- View Bookings -->
						<div id="content_container">
							<div id="full-content">
								<?php include '/modules/view-bookings.php'; ?>
							</div>
						</div>
					<?php else : ?>
						<div id="content-container">
							<?php
								include '/modules/learner-login.php';
							?>
						</div>
					<?php endif; ?>
				<?php elseif($type == 'make-booking') : ?>
					<?php if(is_logged_in()) : ?>
						<!-- Make Booking -->
						<div id="content_container">
							<div id="full-content">
								<h2>Make a Booking</h2>
								<?php include '/modules/make-booking.php'; ?>
							</div>
						</div>
					<?php else : ?>
						<div id="content-container">
							<?php
								include '/modules/learner-login.php';
							?>
						</div>
					<?php endif; ?>
				<?php elseif($type == 'cancel-booking') : ?>
					<?php if(is_logged_in()) : ?>
						<!-- Cancel Bookings -->
						<div id="content_container">
							<div id="full-content">
								<h2>Cancel Booking</h2>
								<?php include '/modules/cancel-booking.php'; ?>
							</div>
						</div>
					<?php else : ?>
						<div id="content-container">
							<?php
								include '/modules/learner-login.php';
							?>
						</div>
					<?php endif; ?>
				<?php elseif($type == 'book') : ?>
					<?php if(is_logged_in()) : ?>
						<!-- Book Lesson -->
						<div id="content_container">
							<div id="full-content">
								<h2>Make a Booking</h2>
								<?php include '/modules/book.php'; ?>
							</div>
						</div>
					<?php else : ?>
						<div id="content-container">
							<?php
								include '/modules/learner-login.php';
							?>
						</div>
					<?php endif; ?>
				<?php elseif($type == 'confirm-booking') : ?>
					<?php if(is_logged_in()) : ?>
						<!-- View Bookings -->
						<div id="content_container">
							<div id="full-content">
								<?php include '/modules/confirm-booking.php'; ?>
							</div>
						</div>
					<?php else : ?>
						<div id="content-container">
							<?php
								include '/modules/learner-login.php';
							?>
						</div>
					<?php endif; ?>
				<?php else : ?>
					<?php include '/modules/error404.php'; ?>
				<?php endif; ?>
			<?php elseif($request=='instructor') : ?>
				<!-- Instructor Request -->
				<div id="navigation">
					<ul>
						<?php if(is_instructor_logged_in()) : ?>
							<li><a href="/action.php?type=logout">Logout</a></li>
							<li><a href="/instructor/view-bookings">View Bookings</a></li>
						<?php else : ?>
							<li><a href="/">Back to Home</a></li>
						<?php endif; ?>
						<img src="/images/nav-bar-guy-red.jpg" alt="Passed Test" id="nav-bar-image" />
					</ul>
				</div>
				<?php if($type=='login') : ?>
					<div id="content-container">	
						<?php include '/modules/instructor-login.php'; ?>
					</div>
				<?php elseif($type=='view-bookings') : ?>
					<div id="content-container">
						<div id="full-content">
							<?php include '/modules/instructor-view-bookings.php'; ?>
						</div>
					</div>
				<?php elseif($type=='cancel-booking') : ?>
					<div id="content-container">
						<div id="full-content">
							<h2>Cancel Booking</h2>
							<?php include '/modules/instructor-cancel-booking.php'; ?>
						</div>
					</div>
				<?php else : ?>
					<?php include '/modules/error404.php'; ?>
				<?php endif; ?>
			<?php else : ?>
				<?php include '/modules/error404.php'; ?>
			<?php endif; ?>
			<!-- Generic Footer -->
			<div id="footer">
				<div class="left">
					<span>Copyright &copy; 2012 Chris James</span>
				</div>
				<div class="right">
					<a href="/instructor/login">Instructor Login</a><span  style="margin:0"> | </span><a href="/privacy-policy">Privacy Policy</a> <span style="margin:0">|</span> <a href="/contact">Contact</a> <span  style="margin:0"> | </span> <a href="/sitemap">Sitemap</a>
				</div>
			</div>
		</div>
	</body>
</html>
<?php 
	$_SESSION['s'] = '';
?>