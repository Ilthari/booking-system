<h2>View Bookings</h2>
<?php 
	$query = "SELECT * FROM learners, bookings WHERE bookings.booking_owner=learners.learner_id ORDER BY `booking_date` ASC LIMIT 0, 10";
	$results = mysql_query($query);
	if(mysql_num_rows($results)>0) {
		while($booking=mysql_fetch_array($results, MYSQL_ASSOC)) {
			echo '<div class="booking">
				<div class="right">
					<a href="/instructor/cancel-booking/'.$booking['booking_id'].'">Cancel Booking</a>
				</div>
				<h3>'.date('H:i \o\n D j M Y', $booking['booking_date']).' for '. $booking['booking_length'] . ' ' .(($booking['booking_length']=='1') ? 'hour' : 'hours') .' with '. $booking['learner_name'] .'</h3>
			</div>';
		}
	}
	else {
		echo '<p>You do not have any current bookings</p>';
	}
?>