<img src="/images/contact-image.png" title="Contact me!" alt="Contact me!" class="left" />
<h2>Contact Me</h2>
<p><b>Fields marked with <span class="required">*</span> are required</b></p>
<form name="contact" action="action.php" method="post">
	<input type="hidden" name="type" value="contact" />
	<table id="contact">
		<tr>
			<td><span>First Name: </span><span class="required">*</span></td>
			<td><input type="text" name="fname" size="25" value="<?php echo $_SESSION['fname']; ?>" /><span class="required"><?php if($_SESSION['fname_error']) echo 'Needs to be filled in.'; ?></span></td>
		</tr>
		<tr>
			<td><span>Last Name: </span><span class="required">*</span></td>
			<td><input type="text" name="lname" size="25" value="<?php echo $_SESSION['lname']; ?>" /><span class="required"><?php if($_SESSION['lname_error']) echo 'Needs to be filled in.'; ?></span></td>
		</tr>
		<tr>
			<td><span>Gender: </span><span class="required"></span></td>
			<td>
				<select name="gender">
					<option value="male" <?php if($_SESSION['gender']=='male') echo 'selected'; ?>>Male</option>
					<option value="female" <?php if($_SESSION['gender']=='female') echo 'selected'; ?>>Female</option>
					<option value="na" <?php if($_SESSION['gender']=='na') echo 'selected'; ?>>Do Not Specify</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><span>Age: </span><span class="required">*</span></td>
			<td>
				<select name="age">
					<?php
						for($i=17; $i<90; $i++) {
							echo '<option value="'.$i.'" '.(($_SESSION['age']==$i) ? 'selected' : '').'>'.$i.'</option>';
						}
					?>
				</select>
				<span class="required"><?php if($_SESSION['age_error']) echo 'You must be 17 or older if you want to drive.'; ?></span>
			</td>
		</tr>
		<tr>
			<td><span>Address: </span><span class="required"></span></td>
			<td><input type="text" name="address" size="25" value="<?php echo $_SESSION['address']; ?>" /></td>
		</tr>
		<tr>
			<td><span>Area: </span><span class="required"></span></td>
			<td><input type="text" name="area" size="25" value="<?php echo $_SESSION['area']; ?>" /></td>
		</tr>
		<tr>
			<td><span>Email Address: </span><span class="required"></span></td>
			<td><input type="text" name="email" size="30" value="<?php echo $_SESSION['email']; ?>" /></td>
		</tr>
		<tr>
			<td><span>Type of Lessons Required: </span><span class="required"></span></td>
			<td>
				<select name="type_of_lessons">
					<option value="beginner" <?php if($_SESSION['type_of_lessons']=='beginner') echo 'selected'; ?>>Beginner</option>
					<option value="advanced" <?php if($_SESSION['type_of_lessons']=='advanced') echo 'selected'; ?>>Advanced</option>
					<option value="passplus" <?php if($_SESSION['type_of_lessons']=='passplus') echo 'selected'; ?>>PassPlus</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><span>Driving License Obtained:</span><span class="required"></span></td>
			<td>
				<input type="radio" name="driving_license" value="yes" <?php if($_SESSION['driving_license']=='yes') echo 'selected'; ?> /> Yes 
				<input type="radio" name="driving_license" value="no" <?php if($_SESSION['driving_license']=='no') echo 'selected'; ?> /> No
			</td>
		</tr>
		<tr>
			<td><input type="submit" value="Get in contact!" class="contact-buttons" /></td>
			<td><input type="reset" value="Reset the form" class="contact-buttons" /></td>
		</tr>
	</table>
</form>
<br class="clear" />
<?php 
	$_SESSION['fname'] = "";
	$_SESSION['lname'] = "";
	$_SESSION['age'] = "";
	$_SESSION['gender'] = "";
	$_SESSION['address'] = "";
	$_SESSION['area'] = "";
	$_SESSION['email'] = "";
	$_SESSION['type_of_lessons'] = "";
	$_SESSION['driving_license'] = "";
	
	$_SESSION['fname_error'] = "";
	$_SESSION['lname_error'] = "";
	$_SESSION['age_error'] = "";
?>