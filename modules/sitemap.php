<h2>Sitemap</h2>
<ul>
	<li><a href="/">Home</a></li>
	<li><a href="/about">About</a></li>
	<li><a href="/services">Services</a>
		<ul>
			<li><a href="/services/pre-test">Theory Test Training</a></li>
			<li><a href="/services/beginner">Beginner</a></li>
			<li><a href="/services/advanced">Post Test Training</a></li>
		</ul>
	</li>
	<li><a href="/contact">Contact</a></li>
	<li><a href="/learner/login">Learner Login</a></li>
	<li><a href="/instructor/login">Instructor Login</a></li>
	<li><a href="/privacy-policy">Privacy Policy</a></li>
</ul>