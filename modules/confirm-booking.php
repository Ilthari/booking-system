<h2>Confirm Booking</h2>
<p>This time period is currently available. Confirm your booking for: </p>
<p><b>Starting: </b><?php echo date('H:i \o\n D j M Y', $_SESSION['start_time']); ?></p>
<p><b>Ending: </b><?php echo date('H:i \o\n D j M Y', $_SESSION['end_time']); ?></p>
<a href="/action.php?type=confirm_booking">Confirm</a> or go <a href="/learner/make-booking">back</a>.