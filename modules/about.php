<h2>About Me</h2>
<p>I have been teaching L-driving for over 34 years, originally in London and now in Milton Keynes, and achieve an average annual pass rate of over 70%, compared to a national average of fewer than 43%. I am rightly proud of this; however, both the instructor and the learner must work hard if they are to achieve their aims.</p>
<p>My instructional qualifications are: -
<ol>
	<li>Department of Transport Approved Driving Instructor exams, theory, practical and instructional passed 1st time (1977).</li>
	<li>DIA/AEB Diploma in Driving Instruction passed 1st time (1995-96)</li>
	<li>DIAmond Advanced Instructor status achieved (1996)</li>
	<li>DSA Approved Fleet Register Trainer (Category B Vehicles) (2003)</li>
</ul>
</p>
<div id="member-organisations">
	<h3>My Member Organisations</h3>
	<img src="/images/organisations/approved_instructor.jpg" alt="Approved Instructor" class="left" />
	<img src="/images/organisations/dia.jpg" alt="DIA" class="left" />
	<img src="/images/organisations/diamond.jpg" alt="diamond" class="left" />
	<img src="/images/organisations/iam.jpg" alt="IAM" class="left" />
	<img src="/images/organisations/passplus.jpg" alt="PassPlus" class="left" />
	<img src="/images/organisations/rospa.jpg" alt="ROSPA" class="left" />
	<br class="clear" />
</div>