<h2>Welcome</h2>

<img src="/images/homepage-image-1.jpg" alt="Passed Test" title="Passed Test"  class="right" />
<p>I have been teaching L-driving for over 34 years, originally in London and now in Milton Keynes, and achieve an average annual pass rate of over 70%, compared to a national average of fewer than 43%. I am rightly proud of this; however, both the instructor and the learner must work hard if they are to achieve their aims.</p>

<img src="/images/homepage-image-2.jpg" alt="Peugeot 207" title="Peugeot 207" class="left" />
<p>My tuition vehicle is a Peugeot 207 turbo-diesel which is very easy to drive, it's docile enough for those who have never been in the driving seat before, yet sprightly enough for when you feel more confident in your driving abilities (anyone can learn to drive if they keep at it!) It has ABS braking, driver's, passenger's and side air bags, and side impact protection system for your safety.</p>
<br style="clear:left" />
