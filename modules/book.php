<?php
	$day = clean($_GET['c']);
	$month = clean($_GET['d']);
	$year = clean($_GET['e']);
	
	if(date('D', mktime(0, 0, 0, $month, $day, $year))!='Sat' && date('D', mktime(0, 0, 0, $month, $day, $year))!='Sun') {
		echo '<p><b>Make a booking for: ' . date('D j M Y', mktime(0, 0, 0, $month, $day, $year)).'</b></p>';
		
		if($_SESSION['s'] == "book_f") {
			echo '<p class="required">This time period has already been taken.</p>';
		}
		elseif($_SESSION['s'] == "book_t") {
			echo '<p class="required">Lessons cannot be booked past 7PM.</p>';
		}
	?>
	<form name="book" action="/action.php" method="post">
		<input type="hidden" value="book" name="type" />
		<input type="hidden" value="<?php echo $day; ?>" name="day" />
		<input type="hidden" value="<?php echo $month; ?>" name="month" />
		<input type="hidden" value="<?php echo $year; ?>" name="year" />
		<span>Time: </span> 
		<select name="hours">
			<?php 
				for($i=10; $i<=18; $i++) {
					$display = (strlen($i)>1) ? $i : '0'.$i;
					echo '<option value="'.$i.'">'.$display.'</option>';
				}
			?>
		</select>
		<select name="minutes">
			<?php 
				for($i=0; $i<=59; $i++) {
					if(($i % 5)==0) {
						$display = (strlen($i)>1) ? $i : '0'.$i;
						echo '<option value="'.$i.'">'.$display.'</option>';
					}
				}
			?>
		</select>
		<span>Length: </span>
		<select name="length">
			<option value="1">1 hour</option>
			<option value="1.5">1.5 hours</option>
			<option value="2">2 hours</option>
		</select>
		<input type="submit" value="Check this time" />
	</form>
<?php } else { ?>
	<p class="required">Bookings are currently not allow on weekends.</p>
	<a href="/learner/make-booking">Go Back</a>
<?php } ?>