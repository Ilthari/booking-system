<h2>Post Test Tuition</h2>
<p>Nowadays it is not wise to think of passing the L-test as the end of learning to drive. It is only a step along the way. Driver training, and therefore knowledge and safety should continue after the test. The following post-test tuition is available: -</p>
<ol>	
	<li>Motorway driving - ( 2 hours)</li>
	<li>Night driving - (1 hour)</li>
	<li>City driving - (2 hours)</li>
	<li>PASS PLUS - all of the above plus rural and adverse weather driving tuition, ( 6 hours). Insurance discounts offset costs *****</li>
	<li>Parking - in car parks and multi-storeys, (2 hours)</li>
	<li>In the garage - how to check tyres, engine oil, brake fluid etc, (I hour)</li>
	<li>DIAmond Advanced Motorist - tuition up to advanced driver standards, (hours depends on ability)</li>
</ol>
<a href="/contact">Interested? Contact Me</a> or go <a href="/services">back to services</a>.