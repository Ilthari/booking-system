<?php
	$month = ($_GET['c']) ? clean($_GET['c']) : date('m') ;
	$year = ($_GET['d']) ? clean($_GET['d']) : date('Y') ;
	$current_month = date('m');
?>
<h3>
	<?php 
		$next_month = ($month==12) ? 1 : $month+1 ;
		$prev_month = ($month==1) ? 12 : $month-1;
		$next_year = ($month==12) ? $year+1 : $year ;
		$prev_year = ($month==1) ? $year-1 : $year ;
		if($month!=$current_month) {
			echo ' <a href="/learner/make-booking/'.$prev_month.'/'.$prev_year.'">&lt;&lt; </a>';
		}
		else {
			echo '&lt;&lt; ';
		}
		echo date('M', mktime(0, 0, 0, $month, 1, $year)) . ' ' . date('Y', mktime(0, 0, 0, $month, 1, $year)); 
		echo ' <a href="/learner/make-booking/'.$next_month.'/'.$next_year.'">&gt;&gt;</a>';
	?>
</h3>
<table id="booking-table">
	<tr class="first-row">
		<td>Monday</td>
		<td>Tuesday</td>
		<td>Wednesday</td>
		<td>Thursday</td>
		<td>Friday</td>
		<td>Saturday</td>
		<td>Sunday</td>
	</tr>
	<?php
		$today = date('j');
		$days_in_month = date('d', mktime(0, 0, 0, ($month+1), 0, $year));
		$offset =  date('N', mktime(0, 0, 0, $month, 1, $year));
		
		$column = 0;
		echo "<tr>";
		for($i=1; $i < $days_in_month+$offset; $i++) {
			if($column <= 6) {
				if($i >= $offset) {
					echo '<td><a href="/learner/book/'.($i-$offset+1).'/'.$month.'/'.$year.'">'.($i-$offset+1).'</a></td>';
				}
				else {
					echo '<td class="offset"></td>';
				}
				$column++;
				
				if($column > 6) {
					$column = 0;
					echo "</tr><tr>";
				}
			}
			else {
				echo '<td>'.($i-$offset).'</td>';
			}
		}
		echo "</tr>";
	?>
</table>