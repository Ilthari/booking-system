<?php
	include '/_config.php';
	include '/includes/functions.php'; 
	
	session_start();
	
	$con = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS);
	$db = mysql_select_db(MYSQL_DB);
	
	$type = clean($_REQUEST['type']);
	
	//Learner Login Action
	if($type == 'learner-login') {
		$username = clean($_POST['username']);
		$password = password_encrypt($_POST['password']);
		
		$query = "SELECT * FROM learners WHERE `learner_username`='$username' && `learner_password`='$password' LIMIT 1";
		$results = mysql_query($query);
		if(mysql_num_rows($results)==1) {
			while($row=mysql_fetch_array($results, MYSQL_ASSOC)) { $data = $row; }
			$_SESSION['id'] = $data['learner_id'];
			$_SESSION['email'] = $data['learner_email'];
			$_SESSION['name'] = $data['learner_name'];
			redirect('/learner/view-bookings');
		}
		else {
			$_SESSION['s'] = 'log_f';
			redirect("/learner/login");
		}
	}
	
	//Instructor Login Action
	if($type == 'instructor-login') {
		$username = clean($_POST['username']);
		$password = password_encrypt($_POST['password']);
		
		$query = "SELECT * FROM instructors WHERE `instructor_username`='$username' && `instructor_password`='$password' LIMIT 1";
		$results = mysql_query($query);
		if(mysql_num_rows($results)==1) {
			while($row=mysql_fetch_array($results, MYSQL_ASSOC)) { $data = $row; }
			$_SESSION['instructor_id'] = $data['instructor_id'];
			$_SESSION['email'] = $data['instructor_email'];
			$_SESSION['name'] = $data['instructor_name'];
			redirect('/instructor/view-bookings');
		}
		else {
			$_SESSION['s'] = 'log_f';
			redirect("/instructor/login");
		}
	}
	
	//Generic Logout Action
	elseif($type == 'logout') {
		session_destroy();
		redirect("/");
	}
	
	//Check booking date Action
	elseif($type == 'book') {
		if(is_logged_in()) {
			$day = clean($_POST['day']);
			$month = clean($_POST['month']);
			$year = clean($_POST['year']);
			$hour = clean($_POST['hours']);
			$mins = clean($_POST['minutes']);
			$length = clean($_POST['length']);
			
			$start = mktime($hour, $mins, 0, $month, $day, $year);
			$end = mktime($hour, $mins, 0, $month, $day, $year) + ($length*3600);
			if(date('H', $end) <= 19) {
				$query ="SELECT * FROM `bookings` WHERE (`booking_date`<='$start' AND `booking_end`>='$start') OR (`booking_date`<= '$end' AND `booking_end`>= '$end')";
				$results = mysql_query($query);
				
				if(mysql_num_rows($results)==0) {
					$_SESSION['start_time'] = $start;
					$_SESSION['end_time'] = $end;
					redirect("/learner/confirm-booking");
				}
				else {
					$_SESSION['s'] = 'book_f';
					redirect("/learner/book/$day/$month/$year");
				}
			}
			else {
				$_SESSION['s'] = 'book_t';
				redirect("/learner/book/$day/$month/$year");
			}
		}
	}
	
	//Confirm Booking Action
	else if($type == 'confirm_booking') {
		if(is_logged_in()) {
			if(isset($_SESSION['start_time'])) {
				$results = mysql_query("SELECT * FROM `bookings` WHERE (`booking_start`<='$start' AND `booking_end`>='$start') OR (`booking_start`>= '$end' AND `booking_end`<= '$end')");
				if(mysql_num_rows($results)==0) {
					$owner = $_SESSION['id'];
					$start = $_SESSION['start_time'];
					$end = $_SESSION['end_time'];
					$length = ($end-$start)/3600;
					
					$query = "INSERT INTO bookings VALUES ('', '$owner', '$start', '$length', '$end')";
					mysql_query($query);
					
					$headers = '';
					$to = $_SESSION['email'];
					$from = "Chris James";
					$message = "You have booked a lesson for ". date('H:i \o\n D j M Y', $start) . " lasting " . $length . " hours";
					mail($headers, $to, $from, $message);
					
					$headers = '';
					$to = INSTRUCTOR_EMAIL;
					$from = "Website Bookings";
					$message = $_SESSION['name'] . " has booked a lesson for ". date('H:i \o\n D j M Y', $start) . " lasting " . $length . " hours";
					mail($headers, $to, $from, $message);
					
					redirect("/learner/view-bookings");
				}
				else {
					redirect("/learner/make-booking");
				}
			}
			else {
				redirect("/learner/make-booking");
			}
		}
	}
	
	//Cancel Booking Action
	elseif($type=='cancel_booking') {
		if(is_logged_in()) {
			$id = $_SESSION['cancel_id'];
			$results = mysql_query("SELECT * FROM `bookings` WHERE `booking_id`='$id' LIMIT 1");
			while($row=mysql_fetch_array($results, MYSQL_ASSOC)) { $data = $row; }
			
			$start = $data['booking_date'];
			
			mysql_query("DELETE FROM bookings WHERE `booking_id`='$id'");
			
			$headers = '';
			$to = INSTRUCTOR_EMAIL;
			$from = "Website Bookings";
			$message = $_SESSION['name'] . " has cancelled a lesson: ". date('H:i \o\n D j M Y', $start);
			mail($headers, $to, $from, $message);
			
			redirect("/learner/view-bookings");
		}
		else if(is_instructor_logged_in()) {
			$id = $_SESSION['cancel_id'];
			$results = mysql_query("SELECT * FROM `bookings`, `learners` WHERE bookings.booking_id='$id' AND learner.learner_id=bookings.booking_owner LIMIT 1");
			while($row=mysql_fetch_array($results, MYSQL_ASSOC)) { $data = $row; }
			
			$start = $data['booking_date'];
			
			mysql_query("DELETE FROM bookings WHERE `booking_id`='$id'");
			
			$headers = '';
			$to = $data['learner_email'];
			$from = INSTRUCTOR_EMAIL;
			$message = "Your instructor has cancelled a lesson on: ". date('H:i \o\n D j M Y', $start) . " If there has been an error, please contact them immediately";
			mail($headers, $to, $from, $message);
			
			redirect("/instructor/view-bookings");
		}
	}
	
	//Contact Form Action
	elseif($type=='contact') {
		$fname = clean($_POST['fname']);
		$lname = clean($_POST['lname']);
		$gender = clean($_POST['gender']);
		$age = clean($_POST['age']);
		$address = clean($_POST['address']);
		$area = clean($_POST['area']);
		$email = clean($_POST['email']);
		$type_of_lessons = clean($_POST['type_of_lessons']);
		$driving_license = clean($_POST['driving_license']);
		
		if(strlen($fname)>=1 && strlen($lname)>1 && $age>=17) {
			//Email to user
			$headers = '';
			$to = $email;
			$from = INSTRUCTOR_EMAIL;
			$message = "Thank you for getting in contact with me via my website. I shall reply in person within the next 24 hours.";
			mail($headers, $to, $from, $message);
			
			//Email to instructor
			$headers = '';
			$to = $email;
			$from = INSTRUCTOR_EMAIL;
			$message = "A user has gotten in contact with you via your website:\n$fname $lname ($age)\nGender: $gender\nAddress: $address, $area\nEmail: $email\nType of Lessons: $type_of_lessons\nDriving License: $driving_license";
			mail($headers, $to, $from, $message);
			
			redirect("/message-sent");
		}
		else {
			//Store text to put it back in the form
			$_SESSION['fname'] = $fname;
			$_SESSION['lname'] = $lname;
			$_SESSION['age'] = $age;
			$_SESSION['gender'] = $gender;
			$_SESSION['address'] = $address;
			$_SESSION['area'] = $area;
			$_SESSION['email'] = $email;
			$_SESSION['type_of_lessons'] = $type_of_lessons;
			$_SESSION['driving_license'] = $driving_license;
	
			//Store what went wrong
			if(strlen($fname)<1) {
				$_SESSION['fname_error'] = true;
			}
			if(strlen($lname)<1) {
				$_SESSION['lname_error'] = true;
			}
			if($age<17) {
				$_SESSION['age_error'] = true;
			}
			//Redirect
			redirect("/contact");
		}
	}
	
?>